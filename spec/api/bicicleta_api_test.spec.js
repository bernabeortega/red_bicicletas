var mongoose = require('mongoose');
var Bicicleta = require('../../models/Bicicleta');
var server = require('../../bin/www');
var request = require('request');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are now connected to test database');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });


    describe('GET Bicicletas /', () => {
        it('Status 200', (done) => {
            // expect(Bicicleta.allBicis.length).toBe(0);
            // var a = new Bicicleta(1, 'negro', 'urbana', [-34.6012424, -58.3861497]);
            // Bicicleta.add(a);
            request.get(base_url, function(error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);

            });
        });
    });

    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {
            var headers = { 'Content-Type': 'application/json' };
            var aBici = '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat":-34, "lng": -54}';

            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicletas;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });
        });
    });

    describe('DELETE BICICLETAS /delete', () => {
        it("status 204", (done) => {
            var a = Bicicleta.createInstance(1, 'negro', 'urbana', [-34.6, -58.8]);
            Bicicleta.add(a, function(err, newBici) {
                var headers = { 'Content-Type': 'application/json' };
                var aBici =
                    '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat":-34, "lng": -54}';

            });
        });
    });
});